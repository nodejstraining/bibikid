activities_chktitle = document.getElementById('title')
activities_chktitle.addEventListener("change", check_title)

function check_title(){
	document.getElementById('loi_title').innerHTML = "";
	var val = activities_chktitle.value
	if(val == ''){
		document.getElementById('loi_title').innerHTML = "Trường bắt buộc.";
	}
}

function Upload(id, title){
	document.getElementById('chan1').innerHTML = 'Upload to ' + title;
	document.getElementById('chan3').value = id;
}

function clickTitleBackground(id){
	var xx = id.toString()
	$.ajax({
		url : './api/getTopicBackground',
		method : 'GET',
		data : {topicId : id},
		success:function(data){
			var background =  JSON.parse(data)
			var show = '';
			if (background != ''){
				for(i in background){
					show += '<div class="article" id="'+background[i]._id+'">'+
				        '<div class="thumb" style="background-image: url('+background[i].url+');"><div class="msg">';
				        	if(background[i].status == 2){
				        		show+='<i class="glyphicon glyphicon-ok"></i>';
				        	}else{
				        		if(background[i].status == 3){
				        			show+='<i class="glyphicon glyphicon-alert"></i>'
				        		}else{
				        			show+='<i class="glyphicon glyphicon-question-sign"></i>'
				        		}
				        		
				        	}
				        show+='</div><div class="title"><i data-toggle="modal" data-target="#myModalshow" onclick ="zumimg(\''+background[i].url+'\')" class="glyphicon glyphicon-zoom-in">  </i> <i data-toggle="tooltip" data-placement="top" title="Xóa ảnh!" class="glyphicon glyphicon-trash" onclick="deleteimgBackground(\''+background[i]._id+'\',\''+background[i].url+'\')"></i></div></div>'+
				    '</div>'
				}
			}else{
				show += '<p>Chưa có dữ liệu</p>';

			}

			document.getElementById("news").innerHTML = show; 
		}
	})
}
function clickTitleCharacter(id){
	var xx = id.toString()
	$.ajax({
		url : './api/getTopicCharacter',
		method : 'GET',
		data : {topicId : id},
		success:function(data){
			var character =  JSON.parse(data)
			var show = '';
			if (character != ''){
				for(i in character){
					show += '<div class="article" id="'+character[i]._id+'">'+
				        '<div class="thumb" style="background-image: url('+character[i].url+');"><div class="msg">';
				        	if(character[i].status == 2){
				        		show+='<i class="glyphicon glyphicon-ok"></i>';
				        	}else{
				        		if(character[i].status == 3){
				        			show+='<i class="glyphicon glyphicon-alert"></i>'
				        		}else{
				        			show+='<i class="glyphicon glyphicon-question-sign"></i>'
				        		}
				        		
				        	}
				        show+='</div><div class="title"><i data-toggle="modal" data-target="#myModalshow" onclick ="zumimg(\''+character[i].url+'\')" class="glyphicon glyphicon-zoom-in">  </i> <i data-toggle="tooltip" data-placement="top" title="Xóa ảnh!" class="glyphicon glyphicon-trash" onclick="deleteimgCharacter(\''+character[i]._id+'\',\''+character[i].url+'\')"></i></div></div>'+
				    '</div>'
					//show +=  '<img onclick="zumimg('+background[i]._id+','+background[i].url+')" id="'+background[i]._id+'" src="'+ background[i].url +'" style="width: 200px; height: 200px; margin: 10px;">';
				}
			}else{
				show += '<p>Chưa có dữ liệu</p>';

			}

			document.getElementById("news").innerHTML = show; 
		}
	})
}

function clickTitleSticker(id){
	var xx = id.toString()
	$.ajax({
		url : './api/getTopicSticker',
		method : 'GET',
		data : {topicId : id},
		success:function(data){
			var sticker =  JSON.parse(data)
			var show = '';
			if (sticker != ''){
				for(i in sticker){
					show += '<div class="article" id="'+sticker[i]._id+'">'+
				        '<div class="thumb" style="background-image: url('+sticker[i].url+');"><div class="msg">';
				        	if(sticker[i].status == 2){
				        		show+='<i class="glyphicon glyphicon-ok"></i>';
				        	}else{
				        		if(sticker[i].status == 3){
				        			show+='<i class="glyphicon glyphicon-alert"></i>'
				        		}else{
				        			show+='<i class="glyphicon glyphicon-question-sign"></i>'
				        		}
				        		
				        	}
				        show+='</div><div class="title"><i data-toggle="modal" data-target="#myModalshow" onclick ="zumimg(\''+sticker[i].url+'\')" class="glyphicon glyphicon-zoom-in">  </i> <i data-toggle="tooltip" data-placement="top" title="Xóa ảnh!" class="glyphicon glyphicon-trash" onclick="deleteimgCharacter(\''+sticker[i]._id+'\',\''+sticker[i].url+'\')"></i></div></div>'+
				    '</div>'
					
				}
			}else{
				show += '<p>Chưa có dữ liệu</p>';

			}

			document.getElementById("news").innerHTML = show; 
		}
	})
}


function deleteimgBackground(id, url){
	$.ajax({
		url : './api/deleteBackground',
		method : 'delete',
		data : {
			backgroundId : id,
			backgroundUrl : url
		},
		success:function(data){  		
			document.getElementById(id).style.display = "none"
		}
	})
}

function deleteimgCharacter(id, url){
	$.ajax({
		url : './api/deleteCharacter',
		method : 'delete',
		data : {characterId : id, characterUrl : url},
		success:function(data){  		
			document.getElementById(id).style.display = "none"
		}
	})
}
function deleteimgSticker(id, url){
	$.ajax({
		url : './api/deleteSticker',
		method : 'delete',
		data : {stickerId : id, stickerUrl: url},
		success:function(data){  		
			document.getElementById(id).style.display = "none"
		}
	})
}

function zumimg(url){
	var imgshow = '<div class="articlezum">'+
				        '<div class="thumbzum" style="background-image: url('+url+');">'+
				        '<div><button type="button" class="close" data-dismiss="modal">&times;</button><div>'+
				        '</div>'+
				    '</div>'
	document.getElementById("show_img_title").innerHTML = imgshow;
}


activities_search = document.getElementById('buttonsearch')
activities_search.addEventListener("click", search)
function search(){
  var valsearch = document.getElementById('inputsearch').value;
  var t = document.getElementsByTagName("table")[0];
  var trs = t.getElementsByTagName("tr");
  var tds = null;
  var i = 1;
  var ck = false;
  for(i; i < trs.length; i++){
    tds = trs[i].getElementsByTagName("td");
    if(valsearch == tds[1].innerHTML || valsearch == tds[2].innerHTML){
      ck = true;
      break;
    }
  }
  if(ck){
     for(var j = 1; j < trs.length; j++){
    if(j != i){
      trs[j].style.display = "none";
    }
  }
  }else{
    document.getElementById("tablecc").innerHTML = "<p style='padding:15px;'>Không có kết quả.</p>";
  }
}


			$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();   
			});
	
